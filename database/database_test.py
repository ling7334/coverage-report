import os
import redis
import unittest
import contextlib
from database.connection import redis_instance, mysql_instance
def unsetenv(key):
    if hasattr(os, 'unsetenv'):
        os.unsetenv(key)
    else:
        os.putenv(key, '')
class TestRedis(unittest.TestCase):
    def setUp(self):
        self.r = redis_instance
    def tearDown(self):
        pass
    def test_log(self):
        unsetenv("REDIS_URL")
        unsetenv("REDIS_PORT")
        self.assertTrue(isinstance(self.r(0), redis.StrictRedis))
        os.environ["REDIS_URL"] = "redis"
        os.environ["REDIS_PORT"] = "6379"
        self.assertTrue(isinstance(self.r(0), redis.StrictRedis))
        unsetenv("REDIS_URL")
        unsetenv("REDIS_PORT")
class TestMYSQL(unittest.TestCase):
    def setUp(self):
        self.mysql = mysql_instance
    def tearDown(self):
        pass
    def test_mysql(self):
        unsetenv("MYSQL_URL")
        unsetenv("MYSQL_PORT")
        unsetenv("MYSQL_USER")
        unsetenv("MYSQL_PWD")
        self.assertTrue(isinstance(self.mysql(), contextlib.closing))
        os.environ["MYSQL_URL"] = "mysql"
        os.environ["MYSQL_PORT"] = "3306"
        os.environ["MYSQL_USER"] = "root"
        os.environ["MYSQL_PWD"] = ""
        self.assertTrue(isinstance(self.mysql(), contextlib.closing))
        unsetenv("MYSQL_URL")
        unsetenv("MYSQL_PORT")
        unsetenv("MYSQL_USER")
        unsetenv("MYSQL_PWD")