import datetime

def timecali(gid=0, cid=0):
    '''
    返回构建好的校时命令
    '''
    gid = format(gid, 'x').zfill(4)
    cid = format(cid, 'x').zfill(8)
    gid = gid[2:4]+gid[0:2]
    cid = cid[6:8]+cid[4:6]+cid[2:4]+cid[0:2]

    dd = datetime.datetime.now()
    return '11' + gid + cid + dd.strftime("%y%m%d") + dd.strftime("%w").zfill(2) + dd.strftime("%H%M%S")

import unittest

class Testassembly(unittest.TestCase):

    def setUp(self):
        self.timecali = timecali

    def tearDown(self):
        pass

    def test_log(self):
        import datetime
        dd = datetime.datetime.now()
        self.assertEqual(self.timecali(),'11000000000000' + dd.strftime("%y%m%d") + dd.strftime("%w").zfill(2) + dd.strftime("%H%M%S"))
