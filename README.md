# Coverage Report Sample


[![build status](https://gitlab.com/ling7334/coverage-report/badges/master/build.svg)](https://gitlab.com/ling7334/coverage-report/pipelines) 
[![coverage report](https://gitlab.com/ling7334/coverage-report/badges/master/coverage.svg)](https://ling7334.gitlab.io/coverage-report/)

Test serve Coverage Report.

## pages
gitlab V8.17 有Bug，配置pages需要额外设置。  
修改/etc/gitlab/gitlab.rb  

    ##! Define to enable GitLab Pages
    pages_external_url "http://YOUR_COMPANY.io/"
    gitlab_pages['enable'] = true
    
    ##! Configure to expose GitLab Pages on external IP address, serving the HTTP
    # gitlab_pages['external_http'] = 8090
    
    ##! Configure to expose GitLab Pages on external IP address, serving the HTTPS
    # gitlab_pages['external_https'] = nil
    
    gitlab_pages['listen_proxy'] = "localhost:8090"
    # gitlab_pages['redirect_http'] = true
    # gitlab_pages['use_http2'] = true
    # gitlab_pages['dir'] = "/var/opt/gitlab/gitlab-pages"
    # gitlab_pages['log_directory'] = "/var/log/gitlab/gitlab-pages"
    gitlab_pages['metrics_address'] = ":9235"

要多配置`gitlab_pages['metrics_address']`才能运行。  

## OmniAuth
如果是内网，auth提供站的callback的重定向会找不到

## Gitlab Runner
使用docker作为私有云runner服务，要配置DNS，如果支持HTTPS，还要添加证书。  

    docker run -d --name gitlab-runner --dns=DNS_IP --restart always \
      -v /var/run/docker.sock:/var/run/docker.sock \
      -v /srv/gitlab-runner/config:/etc/gitlab-runner \
      -v /usr/local/share/ca-certificates/:/usr/local/share/ca-certificates/
      gitlab/gitlab-runner:latest

然后还要更新证书  

    docker exec -it gitlab-runner update-ca-certificate

runner Spawn 出的runner，可能DNS没配置。  
修改`/srv/gitlab-runner/config/config.toml`  
    concurrent = 1
    check_interval = 0
    
    [[runners]]
      name = "Docker Runner"
      url = "https://gitlab.com/ci"
      token = "TOKEN"
      executor = "docker"
      [runners.docker]
        tls_verify = false
        image = "docker:latest"
        dns = ["DNS_IP"]
        privileged = true
        disable_cache = false
        volumes = ["/cache"]
      [runners.cache]

添加`dns = ["DNS_IP"]`j就能解析相应内网域名。

## HTTPS
将自签名证书添加到`/usr/local/share/ca-certificates/`  
并更新`update-ca-certificate`

或者放到`/etc/docker/certs.d/`

## Docker in docker
使用gitlab runner 构建 docker image，要添加`docker-privileged`项
连接时

    docker exec -it gitlab-runner gitlab-runner register -n \
      --url https://gitlab.com/ci \
      --registration-token REGISTRATION_TOKEN \
      --executor docker \
      --description "My Docker Runner" \
      --docker-image "docker:latest" \
      --docker-privileged

